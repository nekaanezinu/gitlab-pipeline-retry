require 'net/http'
require 'json'
require 'yaml'

class Requests
  def initialize(config)
     @config = config
     @http = Net::HTTP.new(@config.url.host, @config.url.port)
     @http.use_ssl = config.url.scheme == 'https' 
  end

  def fetch_merge_requests
    puts "Fetching merge requests..."
    request = init_request
    request.body = Queries.merge_requests_query(@config.project_path, @config.username)
    @http.request(request)
  end

  def retry_pipeline(row)
    puts "Retrying: #{row.title}"
    request = init_request
    request.body = Queries.retry_pipeline_query(row.pipeline_id)
    @http.request(request)
  end

  private

  def init_request
    request = Net::HTTP::Post.new(@config.url.request_uri, 'Content-Type' => 'application/json')
    # request['Authorization'] = "Bearer #{@config.token}"
    request
  end
end

module Queries
  def self.merge_requests_query(project_path, username)
    {
      query: <<-GRAPHQL
        query {
          project(fullPath: "#{project_path}") {
            id
            name
            mergeRequests(authorUsername: "#{username}") {
              nodes {
                id
                title
                headPipeline {
                  id
                  iid
                  status
                }
              }
            }
          }
        }
      GRAPHQL
    }.to_json
  end

  def self.retry_pipeline_query(id)
    {
      query: <<-GRAPHQL
        mutation {
          pipelineRetry(input: {id: "#{id}"}) {
            errors
            pipeline {
              id
              iid
              status
            }
          }
        }
      GRAPHQL
    }.to_json
  end
end

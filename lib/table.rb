require 'terminal-table'

def display_table(state)  
  table = Terminal::Table.new do |t|
    state.rows.each_with_index do |row, index|
      t.add_row row.to_table_row(state.current_row == row)
    end
  end

  system "clear"
  puts table
  puts "k/j - up/down, space - select, return - retry all selected, r - reload, esc - quit"
  puts state.error if state.error
end

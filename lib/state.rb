class State
  attr_accessor :rows, :selected_rows
  attr_reader :error

  def initialize
    @rows = []
    @current_row_index = 0
    @error = nil
  end

  def current_row
    @rows[@current_row_index]
  end

  def selected_rows
    @rows.select { |row| row.selected }
  end

  def parse_merge_requests(response)
    @rows = []
    if response.code != "200"
      @error = response.body
      return
    end

    hash = JSON.parse(response.body)
    hash["data"]["project"]["mergeRequests"]["nodes"].each do |mr|
      @rows << Row.new(mr)
    end 
  end
 
  def down
    @current_row_index = [@current_row_index + 1, @rows.length - 1].min
  end

  def up
    @current_row_index = [@current_row_index - 1, 0].max
  end

  def toggle_select_current
    current_row.toggle_select if current_row.pipeline_id
  end
end

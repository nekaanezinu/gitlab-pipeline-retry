require 'yaml'
require 'uri'

class Config
  CONFIG_PATH = "./config.yml"

  attr_reader :url, :project_path, :username

  def initialize(url, project_path, username)
    @url = url
    @project_path = project_path
    @username = username
  end

  def self.call
    data = YAML.load_file(CONFIG_PATH)
    new(
      URI.parse(data["url"] + "?private_token=#{data["token"]}"),
      data["project_path"],
      data["username"],
    )
  end
end

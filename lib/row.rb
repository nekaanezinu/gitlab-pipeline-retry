class Row
  RETRY_FAILED_STATUS = "RETRY_FAILED"

  attr_reader :pipeline_id, :title, :selected

  def initialize(mr)
    @id = mr["id"]
    @title = mr["title"]
    @pipeline_id = mr.dig("headPipeline", "id")
    @status = mr.dig("headPipeline", "status")
    @selected = false
  end

  def set_status(status)
    @status = status
  end

  def retry_failed
    @status = RETRY_FAILED_STATUS
  end

  def toggle_select
    @selected = !@selected
  end

  def to_table_row(is_current)
    [
       { value: @id.split("/").last },
       { value: @title },
       { value: print_status },
       { value: "[#{@selected ? 'x' : ' '}]" },
       { value: "#{is_current ? '<-' : ""}"}
    ]
  end

  private

  def print_status
    if @status
      @status.colorize(status_color)
    else
      "NO PIPELINE".colorize(:white).on_red
    end
  end

  def status_color
    case @status
    when "CANCELED"
      :yellow
    when "FAILED"
      :red
    when "RETRY_FAILED"
      :red
    else
      :green
    end
  end
end

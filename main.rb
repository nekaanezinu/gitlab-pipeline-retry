require 'tty-prompt'
require 'colorize'
#require 'pry'

require './lib/config.rb'
require './lib/requests.rb'
require './lib/row.rb'
require './lib/state.rb'
require './lib/table.rb'

class Thing
  def initialize
    @config = Config.call
    @state = State.new
    @requests = Requests.new(@config)
    @prompt = TTY::Prompt.new
  end

  def load
    response = @requests.fetch_merge_requests
    @state.parse_merge_requests(response)
    self
  end

  def start
    display_table(@state)

    loop do
      key = @prompt.read_keypress

      case key
      when "j"
        @state.down
      when "k"
        @state.up
      when " "
        @state.toggle_select_current
      when "r"
        load
      when "\r"
        retry_selected
      when "\e"
        break
      end

      display_table(@state)
    end
  end

  private

  def retry_selected
    return unless @state.selected_rows.any?

    @state.selected_rows.each do |row|
      response = @requests.retry_pipeline(row)
      hash = JSON.parse(response.body)
      row.toggle_select

      if hash["data"]["pipelineRetry"]["errors"].any?
        row.retry_failed
      else
        row.set_status(hash["data"]["pipelineRetry"]["pipeline"]["status"])
      end
    end
  end
end

Thing.new.load.start

